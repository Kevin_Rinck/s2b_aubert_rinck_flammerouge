package Jeu;

import Cycliste.*;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author K�vin and Tom
 *
 */
public class Joueur
{
    /**
     * Le nom du joueur
     */
    private String nom;
    
    /**
     * Le rouleur appartenant au joueur
     */
    private Rouleur rouleur;
    
    /**
     * Le sprinteur appartenant au joueur
     */
    private Sprinteur sprinteur;

    /**
     * Constrcuteur permettant de cr�er le joueur et les 2 cyclistes
     * @param n, le nom du joueur
     * @param c_r, le cot� du rouleur
     * @param c_s, le cot� du sprinteur
     * @param pos_r, la position du rouleur
     * @param pos_s, la position du sprinteur
     */
    public Joueur(String n,int c_r,int c_s, int pos_r,int pos_s)
    {
        this.nom=n;
        this.rouleur=new Rouleur();
        this.sprinteur=new Sprinteur();
    }

    public void piocher()
    {
    		Scanner sc = new Scanner(System.in);
            System.out.println(this.nom+", voici les cartes de votre rouleur pour ce tour :");
            ArrayList<Integer> cartes_piocheRouleur = this.rouleur.getPioche();
            
            for(int i=0;i<4;i++)
            {
                System.out.println("--Carte n�"+(i+1)+" : "+cartes_piocheRouleur.get(i));
            }
            int Jouee=sc.nextInt() -1;
            System.out.println("Quelle carte souhaitez-vous jouer pour votre rouleur ?");
            this.rouleur.setCarte_jouee(cartes_piocheRouleur.get(Jouee));
            
            System.out.println(this.nom+", voici les cartes de votre sprinteur pour ce tour :");
            ArrayList<Integer> cartes_piocheSprinteur = this.sprinteur.getPioche();
            for(int i=0;i<4;i++)
            {
                System.out.println("--Carte "+(i+1)+" : "+cartes_piocheSprinteur.get(i));
            }
            System.out.println("Quelle carte souhaitez-vous jouer pour votre sprinteur ?");
            this.rouleur.setCarte_jouee(cartes_piocheSprinteur.get(Jouee));
    }
    
    /**
     * Methode qui recupere le nom du joueur
     * @return le nom du joueur
     */
    public String getNom()
    {
    	String res = nom;
    	return res;
    }   
    
    /**
     * Methode pemettant de recuperer le rouleur du joueur
     * @return Le rouleur
     */
    public Rouleur getRouleur()
    {
    	Rouleur res = rouleur;
        return res;
    }

    /**
     * methode permettant de recuperer le srinteur du joueur
     * @return Le sprinteur
     */
    public Sprinteur getSprinteur()
    {
    	Sprinteur res = sprinteur;
        return res;
    }
}
