package Jeu;

import Circuit.Circuit;
import Cycliste.*;

import java.util.Scanner;

/**
 * @author K�vin and Tom
 *
 */
public class Jeu {
    private Joueur[] joueur;
    private Circuit circuit;

    /**
     * Constructeur de Jeu
     */
    public Jeu() {
        Scanner sc= new Scanner(System.in);

        int nbJoueurs = 0;
        this.circuit = new Circuit();
        while (nbJoueurs <1 && nbJoueurs > 4);
        {
        System.out.println("Veuillez indiquer le nombre de coachs pour la grande course annuelle . 4 coachs maximum");
        nbJoueurs=sc.nextInt();
        }
        
        this.joueur = new Joueur[nbJoueurs];     
    }

    /**
     * M�thode permettant de mettre le jeu en place (saisie des joueurs, initialisation du circuit, ...)
     */
    public void mettreJeuEnPlace()
    {
    	Scanner sc = new Scanner(System.in);
    	int w=0;
    	String[] Identitee = {"Didier","Francine","Marcel","Thierry","Jean-Louis","Francis","Patricia","Marie",};
    	for(int i=0;i<this.joueur.length;i++) {
            System.out.println("Veuillez decliner votre identitee : "+(i+1));
            String nom = sc.next();
            
            int roul = 0;
            System.out.println(nom+" Veuillez indiquer le position de votre rouleur");
            roul=sc.nextInt();
            
            int sprint = 0;
            System.out.println("Desormais veuillez indiquer la position de votre sprinteur");
            
            
            System.out.println("Votre rouleur s'apelle "+Identitee[w]);
            this.joueur[i].getRouleur().setNom(Identitee[w]);
            w++;
            
            int coteR = 0;
            System.out.println("D�finissez le cot� de votre rouleur (0 ou 1)");
            
            int coteS = 0;
            System.out.println("D�finissez le cot� de votre sprinteur (0 ou 1)");
            this.joueur[i] = new Joueur(nom, coteR, coteS, roul, sprint);
            
            System.out.println("Votre sprinteur s'apelle "+Identitee[w]);
            this.joueur[i].getRouleur().setNom(Identitee[w]);
            w++;
            this.circuit.placerCyclisteDepart(this.joueur[i].getRouleur(),roul);
            this.circuit.placerCyclisteDepart(this.joueur[i].getSprinteur(),sprint);
    }
    	this.circuit.CreerCircuit();
    	System.out.println(this.circuit);
    }
    
    /**
     * Methode qui g�re la periode de l'energie
     */
    public void periode_energ()
    {
    	System.out.println("Periode energie en cours !");
    	System.out.println(this.circuit);
        this.piocher_cartes();
    }
    
    /**
     * M�thode s'occupant des mouvements et de la condition de victoire
     * @return Un boolean valant true si un joueur a gagn�, false sinon
     */
    public boolean periode_mvmt()
    {
    	boolean gagner = false;
    	System.out.println("Periode de mouvement en cours !");
    	
    	for(Joueur j : this.joueur)
    	{
    		 this.circuit.mouvement(j.getSprinteur().getNom());
             this.circuit.mouvement(j.getRouleur().getNom());
         }
         System.out.println(this.circuit);
         
         //Test si un joueur est arriv�
         Cycliste c = this.circuit.cycliste_arrivee();
         if(c!=null) {
             gagner=true;
             System.out.println("Le cycliste "+c.getNom()+" a franchi la ligne d'arriv�e.");
             int res = this.retrouver_joueur(c);
             if(res!=0)
                 System.out.println(this.joueur[res].getNom()+" a remport� la course. Bravo !");
         }
         return gagner;
     }
     
     /**
     * Methode permettant le gestion de la pioche des cartes
     */
    private void piocher_cartes() {

         for(int i=0;i<joueur.length;i++)
         {
             this.joueur[i].piocher();
         }
     }
     
     /**
      * M�thode permettant de retrouver un cycliste sur le circuit
     * @param c, le cycliste a cherch�
     * @return L'entier correspondant � sa postion
     */
    private int retrouver_joueur(Cycliste c) {
         boolean trouve=false;
         int res=-1;
         int i=0;
         while(i<this.joueur.length && !trouve)
         {
             if (this.joueur[i].getRouleur().getNom()==c.getNom() || this.joueur[i].getSprinteur().getNom()==c.getNom())
             {
                 res = i;
                 trouve = true;
             } else
                 i++;

         }
         return res;
     }
     
     /**
      * M�thode retournant le joueur sous frome de tableau
     * @return Un tabelau de joueurs
     */
    public Joueur[] getJrs() {
         return this.joueur;
     }

     /**
      * M�thode qui va lancer le jeu et g�rer son bon d�roulement en appelant les autres m�thodes
     */
    public static void main(String[] args)
    {
         Jeu j = new Jeu();
         j.mettreJeuEnPlace();

         boolean victoire = false;
         while (!victoire)
         {
             j.periode_energ();
             victoire = j.periode_mvmt();
         }
     }
}
    
