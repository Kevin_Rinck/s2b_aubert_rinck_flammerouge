package Cycliste;

import java.util.ArrayList;
import java.util.Collections;

/**
 * @author K�vin and Tom
 *
 */
public class Rouleur extends Cycliste
{
	/**
	 * Constructeur de rouleur, ajoute les cartes correspondant au rouleur
	 */
	public Rouleur()
	{
        super();
        this.pioche = new ArrayList<>();
        for (int i=3;i<=7;i++)
        {
        	for (int j = 1; j <= 3; j++) 
        	{
                this.pioche.add(i);
            }
        }
        Collections.shuffle(pioche);
	}
	
	/* 
	 * M�thode permettant, en sortie console, d'afficher une phrase avec le type de cycliste
	 * @return Une phrase explicative du rouleur
	 */
	public String toString()
	{
		String str = ("Le cycliste est un rouleur");
		return(str+super.toString());
	}  
}
