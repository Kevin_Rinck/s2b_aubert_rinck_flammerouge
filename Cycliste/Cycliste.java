package Cycliste;

import java.util.ArrayList;
import java.util.Collections;


/**
 * @author K�vin and Tom
 *
 */
public abstract class Cycliste
{
    /**
     * Le nom du cycliste
     */
    private String nom;
    
    /**
     * La carte qui a �t� jou�
     */
    protected int carte_jouee;
    
    /**
     * Liste contenant la pioche du cycliste
     */
    protected ArrayList<Integer> pioche;
    
    /**
     * Liste contenant la defausse du cycliste
     */
    protected ArrayList<Integer> defausse;

    /**
     * Constructeur qui cr�e une nouvelle d�fausse
     */
    public Cycliste()
    {
        this.defausse = new ArrayList<>();
    }

    /**
     * M�thode m�langeant la d�fausse du cycliste
     */
    public void melanger()
    {
    	for(int i=0;i<this.defausse.size();i++)
    	{
    		this.pioche.add(this.defausse.get(i));
    		this.defausse.remove(i);
    	}
    	Collections.shuffle(this.pioche);
    }
    
    /**
     * M�thode permettant de reinitialiser la liste de cartes du cysliste
     * @param list_cartes, la liste � re-initialiser
     */
    public void reinitialisation(ArrayList<Integer> list_cartes)
    {

        for(int i = 0 ;i< list_cartes.size();i++)
        {
            int attente = this.pioche.indexOf(list_cartes.get(i));
            this.defausse.add(attente);
            this.pioche.remove(attente);
        }
        this.pioche.remove(this.pioche.indexOf(carte_jouee));
        Collections.shuffle(this.pioche);
    }
    
    /**
     * Methode qui va permettre de recuperer la pioche du cycliste
     * @return La liste avec la pioche du cycliste
     */
    public ArrayList<Integer> getPioche()
    {
    	ArrayList<Integer> res = pioche;
    	return res;
    }
    
    /**
     * Methode qui va permettre de recuperer la defausse du cycliste
     * @return La liste avec la defausse du cycliste
     */
    public ArrayList<Integer> getDefausse()
    {
    	ArrayList<Integer> res = defausse;
    	return res;
    }
    
    /**
     * Methode permettant d'appliquer la fatigue du cycliste en lui ajoutant 2 cartes
     */
    public void appliquer_fatigue()
    {
        this.defausse.add(2);
    }

    /**
     * M�thode permettant de recuperer le nom du cycliste
     * @return le nom du cycliste
     */
    public String getNom()
    {
    	String res = nom;
    	return res;
    }
    
    /**
     * M�thode donnant un nom au cycliste
     * @param nom, le nom � lui donner
     */
    public void setNom(String nom)
    {
    	this.nom = nom;
    }
    
    /**
     * Methode recup�rant la carte jou�e
     * @return La carte jou�e
     */
    public int getCarte_jouee()
    {
    	int res = carte_jouee;
        return res;
    }

    /**
     * M�thode permettant de retirer la carte jou�e
     * @param carte_jouee, la carte jou�
     */
    public void setCarte_jouee(int carte_jouee)
    {
        this.carte_jouee = carte_jouee;
    }
}
