package Cycliste;

import java.util.ArrayList;
import java.util.Collections;

/**
 * @author K�vin and Tom
 *
 */
public class Sprinteur extends Cycliste {

    /**
     * Constrcuteur permettant d'ajouter les cartes du sprinteur � la pioche
     */
    public Sprinteur()
    {
        super();
        this.pioche = new ArrayList<>();
        
        for (int i = 2; i <= 5; i++)
        {
            for (int j = 1; j <= 3; j++)
            {
                this.pioche.add(i);
            }
        }
        
        this.pioche.add(9);
        this.pioche.add(9);
        this.pioche.add(9);
        Collections.shuffle(pioche);
    }

    
    public String toString()
	{
		String str = ("Le cycliste est un sprinteur");
		return(str+super.toString());
	}
}
