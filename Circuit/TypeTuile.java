package Circuit;

/**
 * @author K�vin and Tom
 *
 */


/**
 * Constructeur permettant de definir les diff�rents types de tuile.
 */
public enum TypeTuile
{
	DEPART,
    PLAT,
    MONTEE,
    DESCENTE,
    ARRIVEE;
}
