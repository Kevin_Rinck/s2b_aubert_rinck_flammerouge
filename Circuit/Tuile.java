package Circuit;
import Cycliste.*;
import static Circuit.TypeTuile.*;

/**
 * @author K�vin and Tom
 *
 */
public class Tuile
{
	/**
	 * Variable de type cycliste, qui est contenu dans une tuile
	 */
	private Cycliste cycliste;
	
	/**
	 * Variable de type TypeTuile, avec son type (DEPART, PLAT, MONTEE, DESCENTE, ARRIVEE)
	 */
	private TypeTuile typeTuile;
	
	/**
	 * Variable permettant de recuper l'abscisse
	 */
	private int getx;
	
	/**
	 * Variable permettant de recuper l'ordonn�e
	 */
	private int gety;
	
	/**
	 * Constrcteur de tuile
	 */
	public Tuile()
	{
		this.cycliste = null;
		this.typeTuile = PLAT;
	}
	

	/**
	 * M�thode permettant de r�cuperer l'abscisse
	 * @return L'abscisse
	 */
	public int getX()
	{
		return getx;
	}
	
	/**
	 * M�thode permettant de r�cuperer l'ordonn�e
	 * @return L'ordonn�e
	 */
	public int getY()
	{
		return gety;
	}
	
	/**
	 * M�thode permettant de r�cuperer le cyclist
	 * @return Le cycliste
	 */
	public Cycliste getCycliste()
	{
		return cycliste;
	}
	
    /**
     * M�thode qui ve cr�er un cycliste
     * @param cycliste
     */
    public void setCycliste(Cycliste cycliste)
    {
        this.cycliste = cycliste;
    }

    /**
     * M�thode permettant de recuperer le type d'une tuile 
     * @return Le type de la tuile
     */
    public TypeTuile getTypeTuile()
    {
        return typeTuile;
    }

    /**
     * Permet d'appliquer le type d'une tuile � la tuile pass�e en parametre
     * @param typeTuile La tuile � recuperer
     */
    public void setTypeTuile(TypeTuile typeTuile)
    {
        this.typeTuile = typeTuile;
    }
}