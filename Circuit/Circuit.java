package Circuit;
import Cycliste.Cycliste;
import java.util.ArrayList;

/**
 * @author K�vin and Tom
 *
 */

/**
 * @author K�vin
 *
 */
public class Circuit
{
	/**
	 * Tableau des tuiles, qui vont repr�senter le circuit
	 */
	private Tuile[][] circuit;
	
	
	/**
	 * Longueur du circuit, taille de 78
	 */
	public final static int longueur=78;
	
	/**
	 * Constructeur du cicrcuit
	 */
	public Circuit() 
	{
		this.circuit=new Tuile[2][longueur];
		
		//Par defaut le circuit est vide
		for(int i=0;i<2;i++)
		{
			for (int j=0; j<longueur;j++)
			{
				this.circuit[i][j]= new Tuile();
			}
		}
	}
	
	/**
	 * M�thode permettant de cr�er le circuit 
	 */
	public void CreerCircuit()
	{
		for(int i=0;i<5;i++)
		{
			this.circuit[0][i].setTypeTuile(TypeTuile.DEPART);
			this.circuit[1][i].setTypeTuile(TypeTuile.DEPART);
		}
		
		this.circuit[0][9].setTypeTuile(TypeTuile.PLAT);
		this.circuit[1][9].setTypeTuile(TypeTuile.PLAT);
		this.circuit[0][10].setTypeTuile(TypeTuile.DESCENTE);
		this.circuit[1][10].setTypeTuile(TypeTuile.DESCENTE);
		
		for (int i = 18; i < 21; i++)
		{
			this.circuit[0][i].setTypeTuile(TypeTuile.DESCENTE);
			this.circuit[1][i].setTypeTuile(TypeTuile.DESCENTE);
		}
		
		for (int i = 36; i < 40; i++)
		{
			this.circuit[0][i].setTypeTuile(TypeTuile.MONTEE);
			this.circuit[1][i].setTypeTuile(TypeTuile.MONTEE);
		}
		
		for (int i = 40; i < 60; i++)
		{
			this.circuit[0][i].setTypeTuile(TypeTuile.PLAT);
			this.circuit[1][i].setTypeTuile(TypeTuile.PLAT);
		}
		
		for (int i = 60; i < 74; i++)
		{
			this.circuit[0][i].setTypeTuile(TypeTuile.MONTEE);
			this.circuit[1][i].setTypeTuile(TypeTuile.MONTEE);
		}
		
		for (int i = 74; i < 78; i++)
		{
			this.circuit[0][i].setTypeTuile(TypeTuile.ARRIVEE);
			this.circuit[1][i].setTypeTuile(TypeTuile.ARRIVEE);
		}
	}
	
	/**
	 * M�thode permettant de placer le cycliste sur la grille de d�part
	 * @param c, le cycliste � placer
	 * @param pos, sa posisition sur la grille de d�part de longueur 6 (avec le 0)
	 */
	public void placerCyclisteDepart(Cycliste c, int pos)
	{
		if (pos<5)
			this.circuit[0][pos].setCycliste(c);
		else
			this.circuit[1][pos-5].setCycliste(c);
	}
	
	/**
	 * @param nom, le nom du cycliste
	 * @return Un boolean indiquant si le cycliste a boug�
	 */
	public boolean mouvement(String nom)
	{
		boolean res=false;
		//TODO
		return res;
	}
	
	
	/**
	 * M�thode permettant d'appliquer la fatigue aux cyclistes
	 */
	public void fatigue() {
        ArrayList<ArrayList<Integer>> positions = new ArrayList<>();

        //On cherche les cyclistes fatigu�s
        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < longueur; j++)
            {
                if(this.circuit[i][j]!=null && this.circuit[i][j+1]==null && this.circuit[i][j+2]==null)
                {
                    positions.add(new ArrayList<>());
                    positions.get(i).add(j);
                }
            }
        }

        //On donne les fatigues aux cyclistes fatigu�s avec la m�thode appliquer_fatigue
        for(int i=0;i<positions.size();i++) {
            for(int pos : positions.get(i)) {
                this.circuit[i][pos].getCycliste().appliquer_fatigue();
            }
        }
    }
	
	
    /**
     * M�thode gerant le ou les cyclistes � l'arriv�e
     * @return Le cycliste qui est arriv� 
     */
    public Cycliste cycliste_arrivee()
    {
        Cycliste c = null;
        for (int i = 0; i < 2; i++)
        {
            for (int j = longueur-1; j > longueur-5; j--)
            {
                if (this.circuit[i][j] != null)
                {
                    c = this.circuit[i][j].getCycliste();
                }
            }
        }
        return c;
    }

    
  
    /**
     * M�thode permettant de trouver une tuile vide, en fonction d'une tuile donn�e
     * @param depart, la tuile dont on teste si elle est vide
     * @return Une tuile vide, celle du d�part ou celle d'avant si elle n'est pas vide
     */
    public Tuile trouverCaseVide(Tuile depart)
    {
        if (depart!=null)
        {
            if(this.circuit[depart.getY()-1][depart.getX()]!=null)
                return trouverCaseVide(this.circuit[depart.getY()][depart.getX()]);
            else
                return this.circuit[depart.getY()-1][depart.getX()];
        } else
            return depart;
    }
    
    
   
    
    /* 
     * M�thode permettant de faire une sortie console de l'etat du circuit (lieux de mont�es, descentes, ect...)
     * @return Un String avec ce circuit
     */
    public String toString() {
        String res ="";
        for(int i=0;i<this.circuit[0].length;i++) {
            switch(this.circuit[0][i].getTypeTuile()) {
                case DEPART:
                    res+="D";
                    break;
                case PLAT:
                    res+="P";
                    break;
                case MONTEE:
                    res+="M";
                    break;
                case DESCENTE:
                    res+="D";
                    break;
                case ARRIVEE:
                    res+="A";
                    break;
            }
        }

        res+="\n";
        for(int i=0;i<this.circuit[0].length;i++) {
            res+=this.circuit[0][i].toString();
        }
        res+="\n";
        for(int i=0;i<this.circuit[1].length;i++) {
            res+=this.circuit[1][i].toString();
        }
        res+="\n==========================================\n\n";
        return res;
    }
}
	
	
	
	    
